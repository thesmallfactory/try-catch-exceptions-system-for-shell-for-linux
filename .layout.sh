#!/bin/bash

# This script provides, among other funcions,
# functions for the layout of the try-catch exceptions system...

error_message()
{
	echo "ERROR: THE $1 CAN BE APPLIED."
}

text_color()
{
	local error_key_words=(COLOR TEXT)
	local error_messages

	for i in "${!error_key_words[@]}"; do
	error_messages[i]=$(error_message "${error_key_words[$i]}")
	done

	local color_value=$1
	shift
	local text=$1

	if [ ! -z "$color_value" ] && [ -n "$text" ]; then
		echo -e -n "\033[${color_value}m${text}\033[0m"
	else
		if [ -z "$color_value" ]; then
			echo ${error_messages[0]}
		elif [ -z "$text" ]; then
			echo ${error_messages[1]}
		fi
	fi
}

point_counter()
{
	local number=0
	until [ "$number" -eq $1 ]; do
		number=$((++number))
		text_color $2 '.'
		sleep 1
	done
}