#!/bin/bash

# This script provides a try-catch exceptions system.

source .layout.sh

null_value_exception()
{
	text_color 31 "NullValueException: The variable for the $1 is empty.\n"
}

application()
{
	local try="$1"

	local success="$2"
	local failure="$3"


	table=("$try" "$success" "$failure")
}

try_catch()
{
	$1

	if [ -z "${table[0]}" ]; then
		null_value_exception 'function'
	else
		${table[0]}
		if [ $? = 0 ]; then
			if [ -z "${table[1]}" ]; then
				null_value_exception 'success message'
			else
				${table[1]}
			fi
		else
			if [ -z "${table[2]}" ]; then
				null_value_exception 'failure message'
			else
				${table[2]}
			fi
		fi
	fi
}