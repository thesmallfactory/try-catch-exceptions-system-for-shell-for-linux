#!/bin/bash

# This script contains an example to know how to use the try-catch exceptions system...

source .layout.sh
# Loads the system file (named "exception")
source .exception.sh

suspension_points()
{
        point_counter 3 $1
        echo ''
}

stop_the_script()
{
	text_color 31 'The application has just been cancelled: the script will stop... in 3 seconds'; suspension_points 31
}

# Application success case
success_to_test()
{
	text_color "1;32" 'To test is a success'
	suspension_points "1;32"
}
# Application failure case
failure_to_test()
{
	stop_the_script
	exit
}
# Function to test...
to_test()
{
	application "echo 'Hi!'" "success_to_test" "failure_to_test"
}

try_catch "to_test"